import React, { Component } from 'react';

class App extends Component {
  state = {
    heading: "API function using fetch",
    users: [],
    isloading: true,
    isError: false,
  }
  componentDidMount() {
    this.fetchData();
  }
  fetchData() {
    fetch(`https://jsonplaceholder.typicode.com/users/`)
      .then(response => {
        if (response.status >= 200 && response.status <= 299) {
          return response.json();
        } else {
          this.setState({
            isloading: false,
            isError: true,
          })

        }
      }
      )
      .then(data => {
        console.log(data);
        this.setState({
          users: data,
          isloading: false,
        })
      })
  }
  render() {
    if (this.state.isloading) {
      return <div>Loading...</div>
    }
    else if (this.state.isError) {
      return <div>Error...</div>

    }
    else {
      return (
        <>
          {this.state.heading}

          {this.state.users.map(item => {
            return <div key={item.id}>{item.name}</div>
          })}
        </>
      );
    }
  }
}

export default App;